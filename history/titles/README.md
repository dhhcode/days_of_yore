Events
----------
380 Papal Supremacy and Catholocism

Battle of Chalons (451)


376: Visigoths appear on the Danube and are allowed entry into the Roman Empire in their flight from the Huns.
378: Battle of Adrianople: Roman army is defeated by the Visigoth cavalry. Emperor Valens is killed.
378–395: Theodosius I, Roman emperor, bans pagan worship, Christianity is made the official religion of the Empire.
378: Siyaj K'ak' conquers Waka on (January 8), Tikal (January 16) and Uaxactun.

Wall painting of the Council of Constantinople (381) in the Stavropoleos monastery, Romania
381: First Council of Constantinople reaffirms the Christian doctrine of the Trinity by adding to the creed of Nicaea.
383: Battle of Fei River in China.
395: The Battle of Canhe Slope occurs.
395: Roman emperor Theodosius I dies, causing the Roman Empire to split permanently.
Late 4th century: Cubiculum of Leonis, Catacomb of Commodilla, near Rome, is made.
Late 4th century: Atrium added in the Old St. Peter's Basilica, Rome.
Inventions, discoveries, introductions
The Stirrup was invented in China, no later than 322.[6][1]
Kama Sutra, dated between c. 400 BC to c. 300 AD.[7][8]
Iron pillar of Delhi, India is the world's first Iron Pillar.[citation needed]
Trigonometric functions: The trigonometric functions sine and versine originated in Indian astronomy.[citation needed]
Codex Sinaiticus and the Codex Vaticanus Graecus 1209, are the earliest Christian bibles.[9][10]
Book of Steps, Syriac religious discourses.[citation needed


380 – 415: Chandragupta II reigns over the golden age of the Gupta Empire.
399 – 412: The Chinese Buddhist monk Faxian sails through the Indian Ocean and travels throughout Sri Lanka and India to gather Buddhist scriptures.
401: Kumarajiva, a Buddhist monk and translator of sutras into Chinese, arrives in Chang'an.
Early 5th century: Baptistry of Neon, Ravenna, Italy, is built.
405: Mesrop Mashtots introduces number 36 of the 38 letters of the newly created Armenian Alphabet.
406: The eastern frontier of the Western Roman Empire collapses as waves of Suebi, Alans, and Vandals cross the then-frozen river Rhine near Mainz and enter Gaul.
407: Constantine III leads many of the Roman military units from Britain to Gaul and occupies Arles (Arelate). This is generally seen as Rome's withdrawal from Britain.
410: Rome ransacked by the Visigoths led by King Alaric.
411: Suebi establish the first independent Christian kingdom of Western Europe in Gallaecia.
413: St. Augustine, Bishop of Hippo, begins to write The City of God.
415 – 455: Kumaragupta, Gupta emperor.
420: The Jin dynasty comes to an end by Liu Yu.
420 – 589: Northern and Southern dynasties in China.
426: K'inich Yax K'uk' Mo' reestablish Copan.

Copan
431: First Council of Ephesus, the third ecumenical council which upholds the title Theotokos or "mother of God", for Mary, the mother of Jesus Christ.
439: Vandals conquer Carthage.
At some point after 440, the Anglo-Saxons settle in Britain. The traditional story is that they were invited there by Vortigern.
450: Historical linguist Albert C. Baugh dates Old English from around this year.[1]
450: Several stone inscriptions are made witness to edicts from West Java. Amongst others, the Tugu inscription announces decrees of Purnavarman, the King of Tarumanagara, one of the earliest Hindu kingdoms of Java.[2] (up until the year 669)
451: Council of Chalcedon, the fourth ecumenical council which taught of Jesus Christ as one divine person in two natures.
451: The Persians declare war on the Armenians.
451: The Huns under Attila facing the Romans and the Visigoths are defeated in the Battle of Chalons.[3]
452: The Metropolis of Aquileia is destroyed by Attila and his army.
452: Pope Leo I meets in person with Attila on the Mincio River and convinces him not to ransack Rome.
453: Death of Attila. The Hunnic Empire is divided between Attila's sons.
454: Battle of Nedao. Germanic tribes destroy the main Hunnic army and do away with Hunnic domination.
455: Vandals sack Rome..
455 – 467: Skandagupta, the last great Gupta emperor.
469: Death of Dengizich, last Khan of the Hunnic Empire.
470: Riothamus, King of the Britons, helps the Roman Emperor in Brittany against the Visigoths.
476: Deposition of Romulus Augustulus by Odoacer: traditional date for the Fall of Rome in the West.
477 or 495: Chan Buddhists found the Shaolin Monastery on Mount Song in Henan, China.

Romulus Augustus, Last Western Roman Emperor
480: Assassination of Julius Nepos, the last de jure Emperor of the Western Roman Empire, in Dalmatia.
481: Clovis I becomes King of the Western Franks upon the death of Childeric I.
482: Territory of modern Ukraine is established in Kiev.[4]
486: Clovis defeats Syagrius and conquers the last free remnants of the Western Roman Empire.
490: (approximate date) Battle of Mount Badon. According to legend, British forces led by King Arthur defeated the invading Saxons.
491: King Clovis I defeats and subjugates the Kingdom of Thuringia in Germany.
493: Theodoric the Great ousts Odoacer to become King of Italy.
494: Northern Gaul is united under the Frankish King Clovis I, founder of the Merovingian dynasty.
496: Battle of Tolbiac. King Clovis subjugates the Alamanni, and is baptized as a Catholic with a large number of Franks by Remigius, bishop of Reims.
Buddhism reaches Burma and Indonesia.
African and Indonesian settlers reach Madagascar.
The Hopewell tradition comes to an end in North America.
Tbilisi is founded by King Vakhtang Gorgasali.
Inventions, discoveries, introductions
Horse collar invented in China
Chaturang, the precursor of chess, originated in India
Heavy plow in use in Slavic lands
First instance of a metal horseshoe found in Gaul
Anglo-Saxon runes alphabet introduced in England
Charkha (spinning wheel) originated in India
Armenian alphabet created by Mesrob Mashtots c. 405
The first use of Zero found in Bakhshali manuscript in India