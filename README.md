---
documentclass: scrartcl
geometry: margin=2cm
output: pdf_document
lang: cs
---

# Days of Yore - Eurasia and North-Africa in 376CE
Days of Yore is a non-total-conversion mod for Crusader Kings 3 that allows the player to assume the role of one of the leaders or lords of the year 376CE. It attempts to non-obtrusively model the political and cultural events for players and AI without interfering with other bookmarks. It is heavily inspired by [When The World Stopped Making Sense](https://ck3.paradoxwikis.com/When_the_World_Stopped_Making_Sense) mod for Crusader Kings 2 and Crusader Kings 3. I have endeavored to accurately represent the early migrationary time period but there may be errors, omissions and oversights.  In fact I'm sure there are.  Feel free to point them out on our Discord server!

This is a one fella show at this moment but I'll be using the royal "We" throughout this document. 

- [Installation](#installation)
- [Discord](#discord)
- [Coming Real Soon™](#plans)

## Features
- A Roman Empire, teetering on the brink of collapse due to decades of civil wars, depopulation and decentralization of imperial power.
- A non-Roman world that nonetheless is inextricably tied by fate to Rome and the other massive empires of this time.
- Histories seldom told of the massive empires to the East of Europe among the steppes, deserts, mountains and valleys of greater Eurasia.
- Africa of Antiquity, researched and implemented as faithfully as possible.


### License
MIT License - Feel free to copy or fork and extend the Days of Yore repo.  The Days of Yore project makes no warranties or guarantees.  A shout-out in any derivative work would be pretty cool if you did.
You can check out the license.txt file in this directory if you are unfamiliar with the free software terms of the MIT License.


## <a name="installation"></a>Installing this mod
Currently the only method of distribution is this git repo and the only method of installation is manual installation in your mod folder.  When we hit MVP status for this mod we'll release to the Steam Workshop and investigate if other distribution methods are relevant or desired.


### Installing the repo
You will have to install git as a prerequisite if you haven't already.

```
cd [YOUR MOD FOLDER LOCATION]
git clone https://gitlab.com/dhhcode/days_of_yore.git
```


## <a name="plans"></a>Near-term plans
I want to release a Minimum Viable Product (MVP) version of the mod as soon as possible that includes correct cultures and political configuration and at least the basic mechanics of the Roman Empire's internal and foreign politics and complementary tribal mechanics (Foederatus and Tribute/Tributary mechanics)/

### Roadmap
Basic outline of planned work and the order it will occur in.

uncompleted ◻️

completed ✅

canceled ❌

- ✅ DOY-00002 Update README.md with basic info
- ◻️DOY-00003 Finish filling out Indian sub-continent cultures and political arrangement
- ◻️DOY-00004 Finish filling out Central and East Asian cultures and political arrangement
- ◻️DOY-00005 Revisit European and African cultures, diversify Germanic tribal cultures.
- ◻️DOY-00006 Update Roman government and special Senatorial rules
- ◻️DOY-00007 Foederati, Tribal conspiracy and general Tribute and Tributary mechanics
- ◻️DOY-00008 Migration mechanics, weather and diplomatic/martial event driven
- ◻️DOY-00009 Add Roman Court intrigue events
- ◻️DOY-00010 Hun Stuff
- ◻️DOY-00011 Establishment of the Church of the East/Nestorian Church events, Nicene Ecumenical Council events (First Council of Constantinople, Council of Ephesus. etc)

## Long-term plans
The Days of Yore mod has no special/culturally-appropriate clothing assets for the time period being modeled. In addition, cultures and ethnicity are largely built on vanilla CK3.  My most personally desired enhancement (and beyond my personal skills) would be adding culturally-appropriate attire to improve historical accuracy and immersion in the period.  Second to this would be to expand the genetic ethnic definitions to encompass a wider and more representative ethnic implementation of African, Asian and European populations of this time and those to come.

For one example among others, Mongols and Turkic peoples are closely linked via language but not genetically according to recent studies. Vanilla CK3 represents both groups generically as "Asian" like the rest of East Asia in large part.

Additionally I'd like to provide localization in terms of representing cultures and peoples by the names they themselves used when known, as well as localization in the general sense for users whose primary language is not English. I will definitely have to lean on volunteers for localization to non-English languages or we'll get machine-quality translations which I'd probably rather avoid.


## Support
I don't have the time to offer personal support but you can connect to our [discord](#discord) listed below and leave a bug-report or possibly find out some information from myself or others that will help you with your own situation.

## Contributing
We'd love to have you contribute to the project, whether that's pointing out things we've overlooked historically or culturally, other sorts of suggestions and even direct pull-requests to the repo if you feel up for it.  Adding functionality or content, improving existing functionality or content, helping out with localization or any other helpful contribution will be gratefully accepted. Please reach out at our [discord](#discord) to learn more or get in touch.

### Suggestions and Feedback
If you're interested in Days of Yore or CK3 you are probably passionate about history and strategic games.  We'd love to hear any suggestions or general feedback from you on our [discord](#discord), but we can't promise any specific outcome for your requests or feedback.  Any feedback offered to the Days of Yore project is assumed to be freely offered with no expectation of compensation or consideration other than a shout-out for your great input in making the mod better for everyone.

### <a name="discord"></a>Discord
You can reach the project via discord: [https://discord.gg/uRRKnw6jXz](https://discord.gg/uRRKnw6jXz)
I currently treat it as an asynchronous communication medium so you may find me online there occasionally but the bulk of my time for this project is dedicated to finishing the functionality and features mentioned both in the [Near-Term and Roadmap and Long-Term](#plans) plans.  So leave us a message and we'll try to at least respond within a reasonable amount of time.