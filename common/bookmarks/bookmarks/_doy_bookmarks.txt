﻿bm_376_days_of_yore = {
	start_date = 376.1.1
	is_playable = yes
	group = bm_group_376

	weight = {
		value = 90
	}

	character = {
		name = "bm_days_of_yore_valens"
		history_id = 145232
		# Flavius Iulius Valens Augustus
		dynasty = 1022193
		dynasty_splendor_level = 3
		type = male
		birth = 328.1.1
		title = e_byzantium
		government = feudal_government
		culture = roman
		religion = nicene
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		position = { 1100 380 }
	}
}

bm_376_days_of_yore_asia = {
	start_date = 376.1.1
	is_playable = yes
	group = bm_group_376

	weight = {
		value = 90
	}

	character = {
		name = "bm_days_of_yore_gupta"
		history_id = 990080
		dynasty = dynn_doy_gupta
		dynasty_splendor_level = 2
		type = male
		birth = 335.1.1
		title = e_gupta
		government = feudal_government
		culture = gupta
		religion = vaishnavism
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		position = { 1100 380 }
	}
}